app.
filter('domain', function() {
	return function (url) {
	  	return url.split('//')[1].split('/')[0];
	}
}).
filter('cleanLimitTo', function() {
	return function (s) {
	  	return (s.substr(s.length-2, s.length).indexOf('.')>=0) ? s : s + ' ...';
	}
}).
filter('date', function() {
	return function (t) {
	  	return moment(t/1000, "X").format('D MMM YYYY');
	}
}).
filter('lower', function() {
	return function (s) {
	  	return s.toLowerCase();
	}
});