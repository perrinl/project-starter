<?php 
ob_start("ob_gzhandler");
$path = explode('/', dirname(__FILE__));
$dirname = $path[count($path) - 1];
$frontUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' .$dirname . '';
$backUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/' .$dirname . '/php';
$frontPath = dirname(__FILE__) . '';
$backPath = dirname(__FILE__) . '/php';

// Compile Less files
require_once($backPath . '/lib/lessphp/lessc.inc.php');
try {
	lessc::ccompile($frontPath . '/css/styles.less', $frontPath . '/css/styles.css');
} catch (exception $ex) {
	exit('lessc fatal error:'.$ex->getMessage());
}

?>

<!doctype html>
<html ng-app="app">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Language" content="fr-FR" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

		<title>TITLE</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="" />
		<meta name="copyright" content="©" />

		<!-- 
		<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png" />
		<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png" />
		<link rel="icon" type="image/png" href="favicon-192x192.png" sizes="192x192" />
		<link rel="icon" type="image/png" href="favicon-160x160.png" sizes="160x160" />
		<link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
		<meta name="msapplication-TileColor" content="#da532c" />
		<meta name="msapplication-TileImage" content="mstile-144x144.png" />
		-->
		
		<link rel="stylesheet" type="text/css" href="<?php echo($frontUrl); ?>/css/styles.css" />

	</head>


	<body ng-controller="MainCtrl" ng-cloak screen-watch>

		<!-- LIBRARIES -->
		<script src="<?php echo($frontUrl); ?>/js/lib/lodash.compat.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/ua-parser.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/jquery.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/jquery.connectors.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/angular.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/angular-sanitize.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/moment-with-locales.min.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/lib/modernizr.dev.js"></script>

		<!-- APP -->
		<script src="<?php echo($frontUrl); ?>/js/app.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/controlers.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/filters.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/directives.js"></script>
		<script src="<?php echo($frontUrl); ?>/js/layout.js"></script>
	</body>
</html>
<?php ob_end_flush();?>